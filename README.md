# Gandi Exporter

This exporter collects metrics from Gandi API v5. For now, only domains
dates are monitored by it.

## Installation

```bash
virtualenv /path/to/gandi-exporter
source /path/to/gandi-exporter/bin/activate
git clone git@gitlab.com:myelefant1/prometheus-exporters/gandi-api-exporter.git
cd gandi-api-exporter
pip install -r requirements.txt
```

## Execute exporter

gandi-exporter handle requests through **uwsgi**. You need your Gandi API key to
start it.

```bash
/path/to/gandi-exporter/bin/uwsgi --http <listen_address>:<listen_port> --wsgi-file src/app.py --callable app --pyargv <gandi_api_key>
```

## Make requests

`gandi-api-eporter` autodiscovers registered Gandi domains. You just need
to request the `/probe` metrics path through Prometheus.

### Example

```yaml
- job_name: "gandi_api"
  scrape_interval: 60s
  metrics_path: /probe
  static_configs:
    - targets:
      - 127.0.0.1:9340
```

## Metrics

| metric name                      | description                   | type  | value          |
|----------------------------------|-------------------------------|-------|----------------|
| gandi_domain_created_at          | domain creation date          | gauge | UNIX timestamp |
| gandi_domain_registry_created_at | domain registry creation date | gauge | UNIX timestamp |
| gandi_domain_ends_at             | domain registry end date      | gauge | UNIX timestamp |
| gandi_domain_updated_at          | domain update date            | gauge | UNIX timestamp |
